CREATE TABLE "ADMINISTRATOR" (
"id" int4 NOT NULL,
"username" varchar NOT NULL,
"password" varchar NOT NULL,
"enabled" bool NOT NULL,
PRIMARY KEY ("id") 
);

CREATE TABLE "PARAMETER" (
"id" int8 NOT NULL,
"user_id" int8 NOT NULL,
"name" varchar(255) NOT NULL,
"value" varchar(255) NOT NULL,
"enabled" bool NOT NULL,
PRIMARY KEY ("id") 
);

CREATE TABLE "TAG" (
"id" int8 NOT NULL,
"name" int8 NOT NULL,
"enabled" bool NOT NULL,
PRIMARY KEY ("id") 
);

CREATE TABLE "ADVERTISEMENT" (
"id" int8 NOT NULL,
"title" varchar(255) NOT NULL,
"description" varchar(255) NOT NULL,
"start_date" varchar(255) NOT NULL,
"end_date" varchar(255) NOT NULL,
"image" bytea,
"active" bool NOT NULL,
PRIMARY KEY ("id") 
);

CREATE TABLE "USER" (
"id" int8 NOT NULL,
"username" varchar(255) NOT NULL,
"last_name" varchar(255),
"first_name" varchar(255),
"email" varchar(255) NOT NULL,
"password" varchar(255) NOT NULL,
"authentication_type" char(1) NOT NULL,
"enabled" bool NOT NULL,
PRIMARY KEY ("id") 
);

CREATE TABLE "COMPANY" (
"id" int8 NOT NULL,
"name" varchar(255) NOT NULL,
"description" varchar(255),
"logo" bytea,
"enabled" bool NOT NULL,
PRIMARY KEY ("id") 
);

CREATE TABLE "BRANCH_OFFICE" (
"id" int8 NOT NULL,
"company_id" int NOT NULL,
"name" varchar(255),
"address" varchar(255),
"telephone" varchar(255),
"latitude" float8 NOT NULL,
"longitude" float8 NOT NULL,
"enabled" bool NOT NULL,
PRIMARY KEY ("id") 
);

CREATE TABLE "ADVERTISEMENT_TAG" (
"id" int8 NOT NULL,
"advertisement_id" int8 NOT NULL,
"tag_id" int8 NOT NULL,
"enabled" bool NOT NULL,
PRIMARY KEY ("id") 
);

CREATE TABLE "CATALOGUE" (
"id" int8 NOT NULL,
"name" varchar NOT NULL,
"description" varchar,
"enabled" bool NOT NULL,
PRIMARY KEY ("id") 
);

CREATE TABLE "CATALOGUE_DETAIL" (
"id" int8 NOT NULL,
"catalogue_id" int8 NOT NULL,
"value" varchar NOT NULL,
"description" varchar,
"active" bool NOT NULL,
PRIMARY KEY ("id") 
);

CREATE TABLE "USER_COMPANY" (
"id" int8 NOT NULL,
"user_id" int8 NOT NULL,
"company_id" int8 NOT NULL,
"enabled" bool NOT NULL,
PRIMARY KEY ("id") 
);

CREATE TABLE "USER_PARAMETER" (
"id" int8 NOT NULL,
"user_id" int8 NOT NULL,
"parameter_id" int8 NOT NULL,
"enabled" bool NOT NULL
);

CREATE TABLE "USER_TAG" (
"id" int8 NOT NULL,
"user_id" int8 NOT NULL,
"tag_id" int8 NOT NULL,
"enabled" bool NOT NULL,
PRIMARY KEY ("id") 
);

CREATE TABLE "USER_ADVERTISEMENT" (
"id" int8 NOT NULL,
"user_id" int8 NOT NULL,
"advertisement_id" int8 NOT NULL,
"enabled" bool NOT NULL,
PRIMARY KEY ("id") 
);


ALTER TABLE "BRANCH_OFFICE" ADD CONSTRAINT "fk_brach_office-company" FOREIGN KEY ("company_id") REFERENCES "COMPANY" ("id");
ALTER TABLE "ADVERTISEMENT_TAG" ADD CONSTRAINT "fk_advertisement_tag-advertisement" FOREIGN KEY ("advertisement_id") REFERENCES "ADVERTISEMENT" ("id");
ALTER TABLE "ADVERTISEMENT_TAG" ADD CONSTRAINT "fk_advertisement_tag-tag" FOREIGN KEY ("tag_id") REFERENCES "TAG" ("id");
ALTER TABLE "USER_COMPANY" ADD CONSTRAINT "fk_user_company-user" FOREIGN KEY ("user_id") REFERENCES "USER" ("id");
ALTER TABLE "USER_COMPANY" ADD CONSTRAINT "fk_user_company-company" FOREIGN KEY ("company_id") REFERENCES "COMPANY" ("id");
ALTER TABLE "CATALOGUE_DETAIL" ADD CONSTRAINT "fk_catalogue_detail-catalogue" FOREIGN KEY ("catalogue_id") REFERENCES "CATALOGUE" ("id");
ALTER TABLE "USER_PARAMETER" ADD CONSTRAINT "fk_user_parameter-user" FOREIGN KEY ("user_id") REFERENCES "USER" ("id");
ALTER TABLE "USER_PARAMETER" ADD CONSTRAINT "fk_user_parameter-parameter" FOREIGN KEY ("parameter_id") REFERENCES "PARAMETER" ("id");
ALTER TABLE "USER_TAG" ADD CONSTRAINT "fk_user_tag-user" FOREIGN KEY ("user_id") REFERENCES "USER" ("id");
ALTER TABLE "USER_TAG" ADD CONSTRAINT "fk_user_tag-tag" FOREIGN KEY ("tag_id") REFERENCES "TAG" ("id");
ALTER TABLE "USER_ADVERTISEMENT" ADD CONSTRAINT "fk_user_advertisement-user" FOREIGN KEY ("user_id") REFERENCES "USER" ("id");
ALTER TABLE "USER_ADVERTISEMENT" ADD CONSTRAINT "fk_user_advertisement_advertisement" FOREIGN KEY ("advertisement_id") REFERENCES "ADVERTISEMENT" ("id");

