DROP TABLE "User";
DROP TABLE "Advertisement";
DROP TABLE "Parameter";
DROP TABLE "Tag";
DROP TABLE "Branch_Office";
DROP TABLE "Company";
DROP TABLE "Advertisement_Tag";
DROP TABLE "Advertisement_Branch";

CREATE TABLE "User" (
"id" INTEGER NOT NULL,
"username" TEXT NOT NULL,
"last_name" TEXT,
"first_name" TEXT,
"email" TEXT NOT NULL,
"password" TEXT NOT NULL,
"authentication_type" INTEGER NOT NULL,
PRIMARY KEY ("id") 
);

CREATE TABLE "Advertisement" (
"id" INTEGER NOT NULL,
"title" TEXT NOT NULL,
"description" TEXT NOT NULL,
"start_date" TEXT NOT NULL,
"end_date" TEXT,
"image" BLOB,
PRIMARY KEY ("id") 
);

CREATE TABLE "Parameter" (
"id" INTEGER NOT NULL,
"name" TEXT NOT NULL,
"value" TEXT NOT NULL,
PRIMARY KEY ("id") 
);

CREATE TABLE "Tag" (
"id" INTEGER NOT NULL,
"name" TEXT NOT NULL,
PRIMARY KEY ("id") 
);

CREATE TABLE "Branch_Office" (
"id" INTEGER NOT NULL,
"company_id" INTEGER NOT NULL,
"name" TEXT,
"address" TEXT,
"telephone" TEXT,
"latitude" REAL NOT NULL,
"longitude" REAL NOT NULL,
PRIMARY KEY ("id") ,
CONSTRAINT "fk_branch_company" FOREIGN KEY ("company_id") REFERENCES "Company" ("id")
);

CREATE TABLE "Company" (
"id" INTEGER NOT NULL,
"name" TEXT NOT NULL,
"logo" BLOB,
PRIMARY KEY ("id") 
);

CREATE TABLE "Advertisement_Tag" (
"id" INTEGER NOT NULL,
"advertisement_id" INTEGER NOT NULL,
"tag_id" INTEGER NOT NULL,
PRIMARY KEY ("id") ,
CONSTRAINT "fk_advertisement_tag-advertisement" FOREIGN KEY ("advertisement_id") REFERENCES "Advertisement" ("id"),
CONSTRAINT "fk_advertisement_tag-tag" FOREIGN KEY ("tag_id") REFERENCES "Tag" ("id")
);

CREATE TABLE "Advertisement_Branch" (
"id" INTEGER NOT NULL,
"advertisement_id" INTEGER NOT NULL,
"branch_id" INTEGER NOT NULL,
PRIMARY KEY ("id") ,
CONSTRAINT "fk_advertisement_branch_advertisement" FOREIGN KEY ("advertisement_id") REFERENCES "Advertisement" ("id"),
CONSTRAINT "fk_advertisement_branch_branch" FOREIGN KEY ("branch_id") REFERENCES "Branch_Office" ("id")
);

