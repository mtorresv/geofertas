package ec.com.geofertas.app;

import java.util.Timer;
import java.util.TimerTask;

import ec.com.geofertas.R;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;

public class SplashScreenActivity extends Activity {

	private long splashDelay = 3000; //6 seconds
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);
	
	
	TimerTask task = new TimerTask() {
	      @Override
	      public void run() {
	        Intent mainIntent = new Intent().setClass(SplashScreenActivity.this, HomeActivity.class);
	        startActivity(mainIntent);
	        finish(); //We destroy this activity for preventing that user return by pressing back button
	      }
	    };
	    
	    Timer timer = new Timer();
	    timer.schedule(task, splashDelay);//Over six seconds the task is fired
	  
	}
}
