package ec.com.geofertas.app.service.Impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;

import ec.com.geofertas.app.database.GeofertasDBContract;
import ec.com.geofertas.app.database.GeofertasDbHelper;
import ec.com.geofertas.app.drawer.DrawerItem;
import ec.com.geofertas.app.drawer.FavoriteAdvertisementsListItem;
import ec.com.geofertas.app.entities.Advertisement;
import ec.com.geofertas.app.entities.BranchOffice;
import ec.com.geofertas.app.entities.Company;
import ec.com.geofertas.app.entities.User;
import ec.com.geofertas.app.service.GeofertasService;
import ec.com.geofertas.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * Created by whoami on 11/30/15.
 */
public class GeofertasServiceImpl implements GeofertasService {

    private Context mContext;

    private Properties properties;

    private String baseUrl;

    private static final String LOG_TAG = GeofertasServiceImpl.class.getName();

    public GeofertasServiceImpl(Context mContext) {
        this.mContext = mContext;
        initPropertiesFile();
    }

    private void initPropertiesFile(){
        try {
            properties = new Properties();
            InputStream inputStream = mContext.getAssets().open("geofertas.properties");
            properties.load(inputStream);
            baseUrl = properties.getProperty("baseUrl");
        }catch(IOException ex){
            Log.e(this.getClass().getName(), "Error cargando archivo de configuración");
        }
    }

    @Override
    public List<DrawerItem> getMenuOptions(boolean isAuthenticated) {
        List<DrawerItem> outcome = new ArrayList<>();

        if(isAuthenticated){
            DrawerItem drawerItem0 = new DrawerItem();
            outcome.add(drawerItem0);

            DrawerItem drawerItem1 = new DrawerItem("Explorar", "EXP", R.drawable.ic_explore);
            outcome.add(drawerItem1);

            DrawerItem drawerItem2 = new DrawerItem("Favoritos", "FAV", R.drawable.ic_action_heart);
            outcome.add(drawerItem2);


            DrawerItem drawerItem3 = new DrawerItem("Opciones", "OPT", R.drawable.ic_settings);
            outcome.add(drawerItem3);
        }else {
            DrawerItem drawerItem1 = new DrawerItem("Iniciar Sesion", "INI", R.drawable.ic_account_circle);
            outcome.add(drawerItem1);

            DrawerItem drawerItem2 = new DrawerItem("Explorar", "EXP", R.drawable.ic_explore);
            outcome.add(drawerItem2);

            DrawerItem drawerItem3 = new DrawerItem("Opciones", "OPT", R.drawable.ic_settings);
            outcome.add(drawerItem3);
        }
        DrawerItem drawerItemHelp = new DrawerItem("Ayuda", "HLP", R.drawable.ic_help);
        outcome.add(drawerItemHelp);

        return outcome;
    }

    @Override
    public List<FavoriteAdvertisementsListItem> getFavoriteAdvertisements() {
        List<FavoriteAdvertisementsListItem> outcome = new ArrayList<>();

        // public FavoriteAdvertisementsListItem(int id, int icon, String companyName, String advertisementTitle)
        outcome.add(new FavoriteAdvertisementsListItem(1, R.drawable.siente_deporte, "Marathon Sports", "¡Siente el Deporte!"));
        outcome.add(new FavoriteAdvertisementsListItem(2, R.drawable.ic_usfq_alumni, "Isveglio", "Descuentos USFQ Alumni"));
        outcome.add(new FavoriteAdvertisementsListItem(3, R.drawable.ic_nutricalcin, "Sana Sana", "20% de descuento"));
        outcome.add(new FavoriteAdvertisementsListItem(4, R.drawable.ic_eucerin, "Fybeca", "Oferta en Línea Eucerín"));

        return outcome;
    }

    @Override
    public User authenticateUser(String email, String password) {
        User user = authenticateAgainstWS(email, password);

        if(user != null) {
            GeofertasDbHelper geofertasDbHelper = new GeofertasDbHelper(mContext);
            SQLiteDatabase sqLiteDatabase = geofertasDbHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(GeofertasDBContract.User.PROFILE_PICTURE_COLUMN, user.getImage());
            contentValues.put(GeofertasDBContract.User.USERNAME_COLUMN, user.getUsername());
            contentValues.put(GeofertasDBContract.User.FIRST_NAME_COLUMN, user.getFirstName());
            contentValues.put(GeofertasDBContract.User.LAST_NAME_COLUMN, user.getLastname());
            contentValues.put(GeofertasDBContract.User.EMAIL_COLUMN, user.getEmail());
            contentValues.put(GeofertasDBContract.User.PASSWORD_COLUMN, user.getPasssword());
            contentValues.put(GeofertasDBContract.User.AUTHENTICATION_TYPE_COLUMN, user.getAuthenticationType());

            long newRowID = sqLiteDatabase.insert(GeofertasDBContract.User.TABLE_NAME, null, contentValues);

            if (newRowID == -1) {
                return null;
            }
        }
        return user;

    }

    @Override
    public User createAccount(User user){

        User resultUser = createAccountInServer(user);

        if(resultUser != null) {
            GeofertasDbHelper geofertasDbHelper = new GeofertasDbHelper(mContext);
            SQLiteDatabase sqLiteDatabase = geofertasDbHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(GeofertasDBContract.User.PROFILE_PICTURE_COLUMN, resultUser.getImage());
            contentValues.put(GeofertasDBContract.User.USERNAME_COLUMN, resultUser.getUsername());
            contentValues.put(GeofertasDBContract.User.FIRST_NAME_COLUMN, resultUser.getFirstName());
            contentValues.put(GeofertasDBContract.User.LAST_NAME_COLUMN, resultUser.getLastname());
            contentValues.put(GeofertasDBContract.User.EMAIL_COLUMN, resultUser.getEmail());
            contentValues.put(GeofertasDBContract.User.PASSWORD_COLUMN, resultUser.getPasssword());
            contentValues.put(GeofertasDBContract.User.AUTHENTICATION_TYPE_COLUMN, resultUser.getAuthenticationType());

            long newRowID = sqLiteDatabase.insert(GeofertasDBContract.User.TABLE_NAME, null, contentValues);

            if (newRowID == -1) {
                return null;
            }
            return user;
        }
        return null;
    }

    private User createAccountInServer(User user){
        HttpURLConnection urlConnection = null;
        BufferedReader bufferedReader = null;

        // Will contain the raw JSON response as a string.
        String resultJsonString = null;

        try {

            //Gson gson = new Gson();
            //String userJsonToSend = gson.toJson(user);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("username", user.getUsername());
            jsonObject.put("password", user.getPasssword());
            jsonObject.put("lastName", user.getLastname());
            jsonObject.put("picture", Base64.encodeToString(user.getImage(), Base64.DEFAULT));
            jsonObject.put("firstName", user.getFirstName());
            jsonObject.put("enabled", "A");
            jsonObject.put("email", user.getEmail());
            jsonObject.put("authenticationType", "N");
            //final String GEOFERTAS_BASE_URL = "http://10.0.2.2:8080/geofertasService/registerUser/";
            final String wsMethodName= "registerUser";
            Uri uri = Uri.parse(baseUrl + "/" + wsMethodName).buildUpon()
                    .build();

            URL url = new URL(uri.toString());

            Log.v("GeofertasServiceImpl", "BUILT URI: " + uri.toString());

            // Create the request, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
            wr.writeBytes(jsonObject.toString());
            wr.flush();
            wr.close();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                return null;
            }
            String userJsonStr = buffer.toString();
            Log.v(GeofertasServiceImpl.class.getName(), "RESPONSE: " + userJsonStr);
            return parseUserFromJsonStr(userJsonStr);
        } catch (IOException | JSONException e) {
            Log.e("GeofertasServiceImpl", "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attemping
            // to parse it.
            return null;
        } finally{
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (final IOException e) {
                    Log.e("PlaceholderFragment", "Error closing stream", e);
                }
            }
        }

    }

    @Override
    public User updateUserInfo(User user) {

        // TODO reflejar cambios en WS
        User outcomeUser = updateAccountInServer(user);

        if(outcomeUser != null) {

            GeofertasDbHelper geofertasDbHelper = new GeofertasDbHelper(mContext);
            SQLiteDatabase sqLiteDatabase = geofertasDbHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(GeofertasDBContract.User.PROFILE_PICTURE_COLUMN, user.getImage());
            contentValues.put(GeofertasDBContract.User.USERNAME_COLUMN, user.getUsername());
            contentValues.put(GeofertasDBContract.User.FIRST_NAME_COLUMN, user.getFirstName());
            contentValues.put(GeofertasDBContract.User.LAST_NAME_COLUMN, user.getLastname());
            contentValues.put(GeofertasDBContract.User.EMAIL_COLUMN, user.getEmail());
            contentValues.put(GeofertasDBContract.User.PASSWORD_COLUMN, user.getPasssword());

            // Which row to update, based on the ID
            String selection = GeofertasDBContract.User.USERNAME_COLUMN + " = ?";
            String[] selectionArgs = {user.getUsername()};

            long updatedRows = sqLiteDatabase.update(GeofertasDBContract.User.TABLE_NAME, contentValues, selection, selectionArgs);

            if (updatedRows == 1) {
                return user;
            }
        }
        return null;

    }

    private User updateAccountInServer(User user){

        HttpURLConnection urlConnection = null;
        BufferedReader bufferedReader = null;

        try {

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("username", user.getUsername());
            jsonObject.put("password", user.getPasssword());
            jsonObject.put("lastName", user.getLastname());
            jsonObject.put("picture", Base64.encodeToString(user.getImage(), Base64.DEFAULT));
            jsonObject.put("firstName", user.getFirstName());
            jsonObject.put("enabled", "A");
            jsonObject.put("email", user.getEmail());
            jsonObject.put("authenticationType", "N");
            //final String GEOFERTAS_BASE_URL = "http://10.0.2.2:8080/geofertasService/updateUser/";
            final String wsMethodName = "updateUser";
            Uri uri = Uri.parse(baseUrl + "/" + wsMethodName).buildUpon()
                    .build();

            URL url = new URL(uri.toString());

            Log.v("GeofertasServiceImpl", "BUILT URI: " + uri.toString());

            // Create the request, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
            wr.writeBytes(jsonObject.toString());
            wr.flush();
            wr.close();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                return null;
            }
            String userJsonStr = buffer.toString();
            Log.v(GeofertasServiceImpl.class.getName(), "RESPONSE: " + userJsonStr);
            return parseUserFromJsonStr(userJsonStr);
        } catch (IOException | JSONException e) {
            Log.e("GeofertasServiceImpl", "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attemping
            // to parse it.
            return null;
        } finally{
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (final IOException e) {
                    Log.e("PlaceholderFragment", "Error closing stream", e);
                }
            }
        }

    }

    private User parseUserFromJsonStr(String userJsonStr) throws JSONException{
        JSONObject userJson = new JSONObject(userJsonStr);
        /*
        {
          "id": 1,
          "username": "test",
          "lastName": "testlastname",
          "firstName": "testfirstname",
          "email": "testem@il",
          "password": "test",
          "authenticationType": "N",
          "enabled": "A",
          "userCompany": [],
          "userParameter": [],
          "userTag": [],
          "userAdvertisement": []
        }
         */
        String username = userJson.getString("username");
        String lastname = userJson.getString("lastName");
        String firstname = userJson.getString("firstName");
        String email = userJson.getString("email");
        String password = userJson.getString("password");
        byte[] picture = android.util.Base64.decode(userJson.getString("picture"), android.util.Base64.DEFAULT);
        String authenticationType = userJson.getString("authenticationType");

        //User(byte[] image, String username, String lastname, String firstName, String email, String passsword, String authenticationType)
        User user = new User(picture, username, lastname, firstname, email, password, authenticationType);
        return user;
    }


    private User authenticateAgainstWS(String username, String password) {
        HttpURLConnection urlConnection = null;
        BufferedReader bufferedReader = null;

        try {

            final String wsMethodName= "authenticate";
            Uri uri = Uri.parse(baseUrl + "/" + wsMethodName).buildUpon()
                    .appendPath(username)
                    .appendPath(password)
                    .build();

            URL url = new URL(uri.toString());

            Log.v("GeofertasServiceImpl", "BUILT URI: " + uri.toString());

            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            String userJsonStr = buffer.toString();
            Log.v(GeofertasServiceImpl.class.getName(), "RESPONSE: " + userJsonStr);
            return parseUserFromJsonStr(userJsonStr);
        } catch (IOException | JSONException e) {
            Log.e("GeofertasServiceImpl", "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attemping
            // to parse it.
            return null;
        } finally{
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (final IOException e) {
                    Log.e("PlaceholderFragment", "Error closing stream", e);
                }
            }
        }

    }

    @Override
    public Boolean logout(String username) {
        GeofertasDbHelper geofertasDbHelper = new GeofertasDbHelper(mContext);
        SQLiteDatabase sqLiteDatabase = geofertasDbHelper.getWritableDatabase();
        String whereClause = GeofertasDBContract.User.USERNAME_COLUMN + " = ?";
        String[] whereArgs = {username};
        int deletedRows = sqLiteDatabase.delete(GeofertasDBContract.User.TABLE_NAME, whereClause, whereArgs);
        if(deletedRows > 0){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public List<BranchOffice> getBranchOffices(Double lat, Double lon, Double radius) {
        HttpURLConnection urlConnection = null;
        BufferedReader bufferedReader = null;


        try {

            final String wsMethodName= "getBranchOffices";
            Uri uri = Uri.parse(baseUrl + "/" + wsMethodName).buildUpon()
                    .appendPath(String.valueOf(lat))
                    .appendPath(String.valueOf(lon))
                    .appendPath(String.valueOf(radius))
                    .build();

            URL url = new URL(uri.toString());

            Log.v("GeofertasServiceImpl", "BUILT URI: " + uri.toString());

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                return null;
            }
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                return null;
            }
            String branchOfficesJsonStr = buffer.toString();
            Log.v(LOG_TAG, "RESPONSE: " + branchOfficesJsonStr);
            return parseBranchOfficesFromJsonStr(branchOfficesJsonStr);
        } catch (IOException | JSONException e) {
            Log.e("GeofertasServiceImpl", "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attemping
            // to parse it.
            return null;
        } finally{
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (final IOException e) {
                    Log.e("PlaceholderFragment", "Error closing stream", e);
                }
            }
        }
        //return null;
    }

    private List<BranchOffice> parseBranchOfficesFromJsonStr(String branchOfficesJsonStr) throws JSONException{
        JSONArray branchOfficesJsonArray = new JSONArray(branchOfficesJsonStr);
        List<BranchOffice> outcome = new ArrayList<>();
        /*
        {
            "id": 1,
            "name": "Sucursal 6 de Diciembre",
            "address": "Av. 6 de Diciembre y calle AlemÃ¡n",
            "telephone": "2462434",
            "latitude": -0.179337,
            "longitude": -78.478207,
            "enabled": "A",
            "advertisementBranch": [
          {
        */

        for(int i = 0; i < branchOfficesJsonArray.length(); i++){
            Long id = branchOfficesJsonArray.getJSONObject(i).getLong("id");
            String name = branchOfficesJsonArray.getJSONObject(i).getString("name");
            String address = branchOfficesJsonArray.getJSONObject(i).getString("address");
            String telephone = branchOfficesJsonArray.getJSONObject(i).getString("telephone");
            Double latitude = branchOfficesJsonArray.getJSONObject(i).getDouble("latitude");
            Double longitude = branchOfficesJsonArray.getJSONObject(i).getDouble("longitude");

            /*
            "id": 1,
              "name": "Marathon Sports",
              "description": "Empresa deportiva ecuatoriana",
              "logo": "2b77ca28-5622-4f34-b755-aa495bc7b4ce",
              "logoBytes":
                     */
            JSONObject companyJson = branchOfficesJsonArray.getJSONObject(i).getJSONObject("company");

            Long companyId = companyJson.getLong("id");
            String companyName = companyJson.getString("name");
            String companyDescription = companyJson.getString("description");
            byte[] companyLogo = android.util.Base64.decode(companyJson.getString("logoBytes"), android.util.Base64.DEFAULT);

            Company currentCompany = new Company(companyId, companyName, companyDescription, companyLogo);

            JSONArray advertisementBranchJsonArray = branchOfficesJsonArray.getJSONObject(i).getJSONArray("advertisementBranch");

            List<Advertisement> advertisements = null;

            /*
            "advertisement": {
          "id": 1,
          "title": "lLa pasion se ilumina",
          "description": "Compra y personaliza tu camiseta de la seleccion aqui",
          "startDate": "2001-10-05",
          "endDate": "2016-10-02",
          "image": "/adasdsa/adasd.jpg",
          "imageBytes": null,
          "enabled": "A"
        }
             */
            if(advertisementBranchJsonArray != null){
                advertisements = new ArrayList<>();
                for(int j = 0; j < advertisementBranchJsonArray.length(); j++){
                    JSONObject advertisementJson = advertisementBranchJsonArray.getJSONObject(j).getJSONObject("advertisement");
                    Long advertisementId = advertisementJson.getLong("id");
                    String advertisementTitle = advertisementJson.getString("title");
                    String advertisementDescription = advertisementJson.getString("description");
                    // TODO Start date
                    // TODO end date
                    byte[] advertisementImage = android.util.Base64.decode(advertisementJson.getString("imageBytes"), android.util.Base64.DEFAULT);
                    Advertisement advertisement = new Advertisement(advertisementId, advertisementTitle, advertisementDescription, null, null, advertisementImage);
                    advertisements.add(advertisement);
                }
            }

            BranchOffice branchOffice = new BranchOffice(id, name, address, telephone, latitude, longitude, currentCompany, advertisements);

            outcome.add(branchOffice);

        }
        return outcome;
    }
}
