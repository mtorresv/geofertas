package ec.com.geofertas.app.service;

import ec.com.geofertas.app.drawer.DrawerItem;
import ec.com.geofertas.app.drawer.FavoriteAdvertisementsListItem;
import ec.com.geofertas.app.entities.BranchOffice;
import ec.com.geofertas.app.entities.User;

import java.util.List;

/**
 * Created by whoami on 11/30/15.
 */
public interface GeofertasService {

    List<DrawerItem> getMenuOptions(boolean isAuthenticated);

    List<FavoriteAdvertisementsListItem> getFavoriteAdvertisements();

    User authenticateUser(String email, String password);

    User createAccount(User user);

    User updateUserInfo(User user);

    Boolean logout(String username);

    List<BranchOffice> getBranchOffices(Double lat, Double lon, Double radius);

}
