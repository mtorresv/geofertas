package ec.com.geofertas.app;

import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import ec.com.geofertas.R;

/**
 * Fragment that shows the detail info of an advertisement
 * @author seymourdiera
 */
public class AdvertisementDetailFragment extends Fragment {

    // company_logo_imageview
    private ImageView companyLogoImageView;
    // company_name_textview
    private TextView companyNameTextView;
    // share_advertisement_button
    private Button shareAdvertisementButton;
    // save_advertisement_button
    private Button saveAdvertisementButton;
    // fragment_advertisement_detail_advertisement_image_imageview
    private ImageView advertisementImageImageView;
    // fragment_advertisement_detail_advertisement_name_textview
    private TextView advertisementNameTextView;
    // fragment_advertisement_detail_advertisement_description_textview
    private TextView advertisementDescriptionTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_advertisement_detail, container, false);

        companyLogoImageView = (ImageView) view.findViewById(R.id.company_logo_imageview);
        companyNameTextView = (TextView) view.findViewById(R.id.company_name_textview);
        shareAdvertisementButton = (Button) view.findViewById(R.id.share_advertisement_button);
        saveAdvertisementButton = (Button) view.findViewById(R.id.save_advertisement_button);
        advertisementImageImageView = (ImageView) view.findViewById(R.id.fragment_advertisement_detail_advertisement_image_imageview);
        advertisementNameTextView = (TextView) view.findViewById(R.id.fragment_advertisement_detail_advertisement_name_textview);
        advertisementDescriptionTextView = (TextView) view.findViewById(R.id.fragment_advertisement_detail_advertisement_description_textview);


        shareAdvertisementButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, "Revisa la nueva oferta de "
                                +  companyNameTextView.getText()
                        + ": "
                        + advertisementNameTextView.getText()
                        + " #Geofertas");
                        sendIntent.setType("text/plain");
                        startActivity(Intent.createChooser(sendIntent, "Compartir"));
                    }
                }
        );

        return view;
    }
}
