package ec.com.geofertas.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import ec.com.geofertas.app.entities.User;
import ec.com.geofertas.app.service.GeofertasService;
import ec.com.geofertas.app.service.Impl.GeofertasServiceImpl;
import ec.com.geofertas.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ProfileActivity extends DrawerActivity {


    // activity_profile_cancel_button
    private Button cancelButton;
    // activity_profile_accept_button
    private Button acceptButton;
    // activity_profile_user_image_imageview
    private ImageView profileImageView;
    // activity_profile_change_user_image_button
    private Button changePictureButton;
    // activity_profile_delete_user_image_button
    private Button deletePictureButton;
    // activity_profile_username_edittext
    private EditText usernameEditText;
    // activity_profile_name_edittext
    private EditText nameEditText;
    // activity_profile_lastname_edittext
    private EditText lastnameEditText;
    // activity_profile_email_edittext
    private EditText emailEditText;
    // activity_profile_password_edittext
    private EditText passwordEditText;
    // activity_profile_logout_button
    private Button logoutButton;
    // geofertasServic
    private GeofertasService geofertasService;

    private static final int SELECT_IMAGE = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        super.set();

        geofertasService = new GeofertasServiceImpl(this);

        cancelButton = (Button) findViewById(R.id.activity_profile_cancel_button);
        acceptButton = (Button) findViewById(R.id.activity_profile_accept_button);
        profileImageView = (ImageView) findViewById(R.id.activity_profile_user_image_imageview);
        changePictureButton = (Button) findViewById(R.id.activity_profile_change_user_image_button);
        deletePictureButton = (Button) findViewById(R.id.activity_profile_delete_user_image_button);
        usernameEditText = (EditText) findViewById(R.id.activity_profile_username_edittext);
        nameEditText = (EditText) findViewById(R.id.activity_profile_name_edittext);
        lastnameEditText = (EditText) findViewById(R.id.activity_profile_lastname_edittext);
        emailEditText = (EditText) findViewById(R.id.activity_profile_email_edittext);
        passwordEditText = (EditText) findViewById(R.id.activity_profile_password_edittext);
        logoutButton = (Button) findViewById(R.id.activity_profile_logout_button);

        if(super.isAuthenticated()){
            super.retrieveUser();
            Bitmap bmp = BitmapFactory.decodeByteArray(mUser.getImage(), 0, mUser.getImage().length);
            profileImageView.setImageBitmap(bmp);
            usernameEditText.setText(mUser.getUsername());
            nameEditText.setText(mUser.getFirstName());
            lastnameEditText.setText(mUser.getLastname());
            emailEditText.setText(mUser.getEmail());
            passwordEditText.setText(mUser.getPasssword());
        }

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Call Service for updating user
                if(allFieldsValid()) {
                    UpdateProfileTask updateProfileTask = new UpdateProfileTask();
                    updateProfileTask.execute();
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileActivity.this.finish();
            }
        });

        changePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);//
                startActivityForResult(Intent.createChooser(intent, "Seleccione una imagen"), SELECT_IMAGE);
            }
        });


        deletePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileImageView.setImageResource(R.drawable.profile_default);
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO cerrar sesion
                LogoutTask logoutTask = new LogoutTask();
                logoutTask.execute(usernameEditText.getText().toString());
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_IMAGE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                        profileImageView.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "Operación cancelada", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private User updateUserInfo(){
        Bitmap bitmap = ((BitmapDrawable)profileImageView.getDrawable()).getBitmap();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] image = bos.toByteArray();
        if(!mUser.getImage().equals(image)) {
            mUser.setImage(image);
        }
        String name = nameEditText.getText().toString();
        if(!mUser.getFirstName().equals(name)){
            mUser.setFirstName(name);
        }
        String lastname = lastnameEditText.getText().toString();
        if(!mUser.getLastname().equals(lastname)){
            mUser.setLastname(lastname);
        }
        String email = emailEditText.getText().toString();
        if(!mUser.equals(email)){
            mUser.setEmail(email);
        }
        String password = passwordEditText.getText().toString();
        if(!mUser.equals(password)){
            mUser.setPasssword(password);
        }
        GeofertasService geofertasService = new GeofertasServiceImpl(this);
        return geofertasService.updateUserInfo(mUser);
    }

    private boolean allFieldsValid() {
        boolean valid = true;
        if (usernameEditText.getText() == null || usernameEditText.getText().toString().trim().length() == 0) {
            usernameEditText.setError("El campo username es obligatorio");
            valid = false;
        }
        if (nameEditText.getText() == null || nameEditText.getText().toString().trim().length() == 0) {
            nameEditText.setError("El campo nombre es obligatorio");
            valid = false;
        }
        if (lastnameEditText.getText() == null || lastnameEditText.getText().toString().trim().length() == 0) {
            lastnameEditText.setError("El campo apellido es obligatorio");
            valid = false;
        }
        if (emailEditText.getText() == null || emailEditText.getText().toString().trim().length() == 0) {
            emailEditText.setError("El campo email es obligatorio");
            valid = false;
        }
        if (passwordEditText.getText() == null || passwordEditText.getText().toString().trim().length() == 0) {
            passwordEditText.setError("El campo password es obligatorio");
            valid = false;
        }
        return valid;
    }

    private class LogoutTask extends AsyncTask<String, Void, Boolean> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(ProfileActivity.this, "", "Cerrando Sesión...");
        }

        @Override
        protected Boolean doInBackground(String[] params) {
            return geofertasService.logout(params[0]);
        }
        protected void onPostExecute(Boolean result) {
            dialog.dismiss();
            if(!result){
                Toast.makeText(ProfileActivity.this, "No se pudo cerrar la sesión. Intente nuevamente", Toast.LENGTH_SHORT).show();
            }
            else{
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this);
                preferences.edit().putBoolean("isLogin", false).commit();
                mUser = null;
                Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        }
    }

    private class UpdateProfileTask extends AsyncTask<Void, Void, User>{
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(ProfileActivity.this, "", "Guardando cambios...");
        }

        @Override
        protected User doInBackground(Void... params) {
            return updateUserInfo();
        }
        protected void onPostExecute(User result) {
            dialog.dismiss();
            if(result == null){
                Toast.makeText(ProfileActivity.this, "Se presentaron problemas al actualizar el perfil. Intente nuevamente", Toast.LENGTH_SHORT).show();
            }
            else{
                mUser = result;
                Toast.makeText(ProfileActivity.this, "Actualización satisfactoria", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        }
    }

}
