package ec.com.geofertas.app;

import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import ec.com.geofertas.R;

/**
 * Fragmento que presenta los elementos
 */
public class HomeFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        Button btnExplore = (Button) view.findViewById(R.id.btn_explore);
        btnExplore.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startExplore();
                    }
                }
        );

        return view;
    }

    public void startExplore() {
        Intent intent = new Intent(getActivity(), MapActivity.class);
        getActivity().startActivity(intent);
    }
}
