package ec.com.geofertas.app;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import ec.com.geofertas.app.entities.User;
import ec.com.geofertas.app.service.GeofertasService;
import ec.com.geofertas.app.service.Impl.GeofertasServiceImpl;
import ec.com.geofertas.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignInFragment extends Fragment {

    private Button signInButton;
    private TextView mUsernameTextView;
    private TextView mPasswordTextView;
    GeofertasService geofertasService;
    User mUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);

        signInButton = (Button) view.findViewById(R.id.fragment_sign_in_sign_in_button);
        mUsernameTextView = (TextView) view.findViewById(R.id.fragment_sign_in_username_textview);
        mPasswordTextView = (TextView) view.findViewById(R.id.fragment_sign_in_password_textview);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence email = mUsernameTextView.getText();
                final CharSequence password = mPasswordTextView.getText();
                boolean fieldsValidated = true;
                if(email == null || email.toString().trim().isEmpty()){
                    mUsernameTextView.setError(getActivity().getString(R.string.sign_in_fragment_username_mandatory_msg));
                    fieldsValidated = false;
                }
                if(password == null || password.toString().trim().isEmpty()){
                    mPasswordTextView.setError(getActivity().getString(R.string.sign_in_fragment_password_mandatory_msg));
                    if(fieldsValidated) fieldsValidated = false;
                }
                if(fieldsValidated) {
                    geofertasService = new GeofertasServiceImpl(getActivity());
                    AuthenticateTask authenticateTask = new AuthenticateTask();
                    authenticateTask.execute(email.toString(), password.toString());
                }
            }
        });

        return view;

    }

    private class AuthenticateTask extends AsyncTask<String, Void, User> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(SignInFragment.this.getActivity(), "", "Autenticando....");
        }

        @Override
        protected User doInBackground(String... params) {
            return geofertasService.authenticateUser(params[0], params[1]);
        }
        protected void onPostExecute(User user) {

            dialog.dismiss();
            if(user == null){
                Toast.makeText(getActivity(), "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
            }
            else{
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                preferences.edit().putBoolean("isLogin", true).commit();

                mUser = user;

                Intent intent = new Intent(getActivity(), HomeActivity.class);
                getActivity().startActivity(intent);
            }
        }
    }

}
