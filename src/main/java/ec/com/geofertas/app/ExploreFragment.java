package ec.com.geofertas.app;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ec.com.geofertas.R;
import com.google.android.gms.maps.GoogleMap;

/**
 * Created by whoami on 1/7/2015.
 */
public class ExploreFragment extends Fragment {


    private static GoogleMap mMap;
    private static Double latitude, longitude;

    private static View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        view = inflater.inflate(R.layout.fragment_explore, container, false);

        return view;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}