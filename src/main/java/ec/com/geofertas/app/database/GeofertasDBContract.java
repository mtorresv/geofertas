package ec.com.geofertas.app.database;

import android.provider.BaseColumns;

/**
 * Created by seymourdiera on 16/10/2015.
 */
public final class GeofertasDBContract {

    public static final String FK_BRANCH_COMPANY = "fk_branch_company";
    public static final String FK_ADVERTISEMENT_TAG_ADVERTISEMENT = "fk_advertisement_tag-advertisement";
    public static final String FK_ADVERTISEMENT_TAG = "fk_advertisement_tag";
    public static final String FK_ADVERTISEMENT_BRANCH_ADVERTISEMENT = "fk_advertisement_branch_advertisement";
    public static final String FK_ADVERTISEMENT_BRANCH_BRANCH = "fk_advertisement_branch_branch";

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public GeofertasDBContract() {
    }

      /* Inner classes that defines the tables contents */

    public static abstract class Advertisement implements BaseColumns {
        public static final String TABLE_NAME = "Advertisement";
        public static final String TITLE_COLUMN = "title";
        public static final String DESCRIPTION_COLUMN = "description";
        public static final String START_DATE_COLUMN = "start_date";
        public static final String END_DATE_COLUMN = "end_date";
        public static final String IMAGE_COLUMN = "image";
    }

    public static abstract class AdvertisementBranch implements BaseColumns {
        public static final String TABLE_NAME = "Advertisement_Branch";
        public static final String ADVERTISEMENT_ID_COLUMN = "advertisement_id";
        public static final String BRANCH_ID_COLUMN = "branch_id";
    }

    public static abstract class AdvertisementTag implements BaseColumns {
        public static final String TABLE_NAME = "Advertisement_Tag";
        public static final String ADVERTISEMENT_ID_COLUMN = "advertisement_id";
        public static final String TAG_ID_COLUMN = "tag_id";
    }

    public static abstract class BranchOffice implements BaseColumns {
        public static final String TABLE_NAME = "Branch_Office";
        public static final String COMPANY_ID_COLUMN = "company_id";
        public static final String NAME_COLUMN = "name";
        public static final String ADDRESS_COLUMN = "address";
        public static final String TELEPHONE_COLUMN = "telephone";
        public static final String LATITUDE_COLUMN = "latitude";
        public static final String LONGITUDE_COLUMN = "longitude";
    }

    public static abstract class Company implements BaseColumns {
        public static final String TABLE_NAME = "Company";
        public static final String NAME_COLUMN = "name";
        public static final String LOGO_COLUMN = "logo";
    }

    public static abstract class Parameter implements BaseColumns  {
        public static final String TABLE_NAME = "Parameter";
        public static final String NAME_COLUMN = "name";
        public static final String VALUE_COLUMN = "value";
    }

    public static abstract class Tag implements BaseColumns {
        public static final String TABLE_NAME = "Tag";
        public static final String NAME_COLUMN = "name";
    }

    public static abstract class User implements BaseColumns {
        public static final String TABLE_NAME = "User";
        public static final String PROFILE_PICTURE_COLUMN = "profile_picture";
        public static final String USERNAME_COLUMN = "username";
        public static final String LAST_NAME_COLUMN = "last_name";
        public static final String FIRST_NAME_COLUMN = "first_name";
        public static final String EMAIL_COLUMN = "email";
        public static final String PASSWORD_COLUMN = "password";
        public static final String AUTHENTICATION_TYPE_COLUMN = "authentication_type";
    }

}
