package ec.com.geofertas.app.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by torresm on 1/20/2015.
 */
public class GeofertasDbHelper extends SQLiteOpenHelper{

    /**
     * Versión de la base de datos
     * Cambiar cada vez que se cambia el esquema de bdd
     */
    public static final int DATABASE_VERSION = 2;

    /**
     * Nombre de la base de datos
     */
    public static final String DATABASE_NAME= "Geofertas.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = "INTEGER";
    private static final String REAL_TYPE = "REAL";
    private static final String BLOB_TYPE = "BLOB";
    private static final String COMMA_SEP = ",";
    private static final String SEMICOLON_SEP = ";";
    private static final String CREATE_TABLE_STATEMENT = "CREATE TABLE ";
    private static final String DROP_TABLE_STATEMENT = "DROP TABLE IF EXISTS ";
    private static final String NOT_NULL = "NOT NULL";
    private static final String PRIMARY_KEY = "PRIMARY KEY";
    private static final String FOREIGN_KEY = "FOREIGN KEY";
    private static final String CONSTRAINT = "CONSTRAINT";
    private static final String REFERENCES = "REFERENCES";


/*
CREATE TABLE "User" (
"id" INTEGER NOT NULL,
"username" TEXT NOT NULL,
"last_name" TEXT,
"first_name" TEXT,
"email" TEXT NOT NULL,
"password" TEXT NOT NULL,
"authentication_type" INTEGER NOT NULL,
PRIMARY KEY ("id")
);
 */
    private static final String CREATE_USER_TABLE_STATEMENT =
            CREATE_TABLE_STATEMENT  + GeofertasDBContract.User.TABLE_NAME   + " ("

            + GeofertasDBContract.User._ID                          + " "   + INTEGER_TYPE      + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.User.PROFILE_PICTURE_COLUMN       + " "   + BLOB_TYPE         + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.User.USERNAME_COLUMN              + " "   + TEXT_TYPE         + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.User.LAST_NAME_COLUMN             + " "   + TEXT_TYPE                             + COMMA_SEP
            + GeofertasDBContract.User.FIRST_NAME_COLUMN            + " "   + TEXT_TYPE                             + COMMA_SEP
            + GeofertasDBContract.User.EMAIL_COLUMN                 + " "   + TEXT_TYPE         + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.User.PASSWORD_COLUMN              + " "   + TEXT_TYPE         + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.User.AUTHENTICATION_TYPE_COLUMN   + " "   + INTEGER_TYPE      + " "   + NOT_NULL  + COMMA_SEP

            + PRIMARY_KEY   + " ("   + GeofertasDBContract.User._ID  + ")"   + ")"   + SEMICOLON_SEP;

    /*
    CREATE TABLE "Advertisement" (
"id" INTEGER NOT NULL,
"title" TEXT NOT NULL,
"description" TEXT NOT NULL,
"start_date" TEXT NOT NULL,
"end_date" TEXT,
"image" BLOB,
PRIMARY KEY ("id")
);
     */
    private static final String CREATE_ADVERTISEMENT_TABLE_STATEMENT =

            CREATE_TABLE_STATEMENT  + GeofertasDBContract.Advertisement.TABLE_NAME +     " ("

            + GeofertasDBContract.Advertisement._ID                 + " "   + INTEGER_TYPE      + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.Advertisement.TITLE_COLUMN        + " "   + TEXT_TYPE         + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.Advertisement.DESCRIPTION_COLUMN  + " "   + TEXT_TYPE         + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.Advertisement.START_DATE_COLUMN   + " "   + TEXT_TYPE         + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.Advertisement.END_DATE_COLUMN     + " "   + TEXT_TYPE                             + COMMA_SEP
            + GeofertasDBContract.Advertisement.IMAGE_COLUMN        + " "   + BLOB_TYPE                             + COMMA_SEP

            + PRIMARY_KEY + " (" + GeofertasDBContract.Advertisement._ID + ")"

            + ")" + SEMICOLON_SEP;

    /*
    CREATE TABLE "Parameter" (
"id" INTEGER NOT NULL,
"name" TEXT NOT NULL,
"value" TEXT NOT NULL,
PRIMARY KEY ("id")
);
     */
    private static final String CREATE_PARAMETER_TABLE_STATEMENT =

            CREATE_TABLE_STATEMENT  + GeofertasDBContract.Parameter.TABLE_NAME  + " ("

            + GeofertasDBContract.Parameter._ID             + " "   + INTEGER_TYPE  + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.Parameter.NAME_COLUMN     + " "   + TEXT_TYPE    + " "    + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.Parameter.VALUE_COLUMN    + " "   + TEXT_TYPE     + " "   + NOT_NULL  + COMMA_SEP

            + PRIMARY_KEY   + " ("   + GeofertasDBContract.Parameter._ID + ")"

            + ")"   + SEMICOLON_SEP;

    /*
    CREATE TABLE "Tag" (
"id" INTEGER NOT NULL,
"name" TEXT NOT NULL,
PRIMARY KEY ("id")
);
     */
    private static final String CREATE_TAG_TABLE_STATEMENT =

            CREATE_TABLE_STATEMENT  + GeofertasDBContract.Tag.TABLE_NAME     + " ("

            + GeofertasDBContract.Tag._ID           + " "   + INTEGER_TYPE  + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.Tag.NAME_COLUMN   + " "   + TEXT_TYPE     + " "   + NOT_NULL  + COMMA_SEP

            + PRIMARY_KEY   + " ("   + GeofertasDBContract.Tag._ID   + ")"

            + ")"   + SEMICOLON_SEP;

    /*
CREATE TABLE "Company" (
"id" INTEGER NOT NULL,
"name" TEXT NOT NULL,
"logo" BLOB,
PRIMARY KEY ("id")
);
 */
    private static final String CREATE_COMPANY_TABLE_STATEMENT =
            CREATE_TABLE_STATEMENT  + GeofertasDBContract.Company.TABLE_NAME    + "("

                    + GeofertasDBContract.Company._ID           + " "   + INTEGER_TYPE  + " "   + NOT_NULL  + COMMA_SEP
                    + GeofertasDBContract.Company.NAME_COLUMN   + " "   + TEXT_TYPE     + " "   + NOT_NULL  + COMMA_SEP
                    + GeofertasDBContract.Company.LOGO_COLUMN   + " "   + BLOB_TYPE                         + COMMA_SEP

                    + PRIMARY_KEY   + " ("   + GeofertasDBContract.Company._ID   + ")"

                    + ")"   + SEMICOLON_SEP;


    /*
    CREATE TABLE "Branch_Office" (
"id" INTEGER NOT NULL,
"company_id" INTEGER NOT NULL,
"name" TEXT,
"address" TEXT,
"telephone" TEXT,
"latitude" REAL NOT NULL,
"longitude" REAL NOT NULL,
PRIMARY KEY ("id") ,
CONSTRAINT "fk_branch_company"
FOREIGN KEY ("company_id")
REFERENCES "Company" ("id")
);
     */
    private static final String CREATE_BRANCH_OFFICE_TABLE_STATEMENT =

            CREATE_TABLE_STATEMENT  + GeofertasDBContract.BranchOffice.TABLE_NAME   + " ("

            + GeofertasDBContract.BranchOffice._ID                  + " "   + INTEGER_TYPE  + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.BranchOffice.COMPANY_ID_COLUMN    + " "   + INTEGER_TYPE  + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.BranchOffice.NAME_COLUMN          + " "   + TEXT_TYPE                         + COMMA_SEP
            + GeofertasDBContract.BranchOffice.ADDRESS_COLUMN       + " "   + TEXT_TYPE                         + COMMA_SEP
            + GeofertasDBContract.BranchOffice.TELEPHONE_COLUMN     + " "   + TEXT_TYPE                         + COMMA_SEP
            + GeofertasDBContract.BranchOffice.LATITUDE_COLUMN      + " "   + REAL_TYPE     + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.BranchOffice.LONGITUDE_COLUMN     + " "   + REAL_TYPE     + " "   + NOT_NULL  + COMMA_SEP

            + PRIMARY_KEY   + " ("   + GeofertasDBContract.BranchOffice._ID  + ")"   + COMMA_SEP
            + CONSTRAINT    + " " + GeofertasDBContract.FK_BRANCH_COMPANY + " "
            + FOREIGN_KEY   + " ("   + GeofertasDBContract.BranchOffice.COMPANY_ID_COLUMN    + ") "
            + REFERENCES    + " "   + GeofertasDBContract.Company.TABLE_NAME    + " ("   + GeofertasDBContract.Company._ID   + ")"
            + ")" + SEMICOLON_SEP;


    /*
    CREATE TABLE "Advertisement_Tag" (
"id" INTEGER NOT NULL,
"advertisement_id" INTEGER NOT NULL,
"tag_id" INTEGER NOT NULL,
PRIMARY KEY ("id") ,
CONSTRAINT "fk_advertisement_tag-advertisement" FOREIGN KEY ("advertisement_id") REFERENCES "Advertisement" ("id"),
CONSTRAINT "fk_advertisement_tag-tag" FOREIGN KEY ("tag_id") REFERENCES "Tag" ("id")
);
     */
    private static final String CREATE_ADVERTISEMENT_TAG_TABLE =
            CREATE_TABLE_STATEMENT  + GeofertasDBContract.AdvertisementTag.TABLE_NAME + "("
            + GeofertasDBContract.AdvertisementTag._ID                      + " "   + INTEGER_TYPE  + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.AdvertisementTag.ADVERTISEMENT_ID_COLUMN  + " "   + INTEGER_TYPE  + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.AdvertisementTag.TAG_ID_COLUMN            + " "   + INTEGER_TYPE  + " "   + NOT_NULL  + COMMA_SEP

            + PRIMARY_KEY   + " ("   + GeofertasDBContract.AdvertisementTag._ID  + ")"   + COMMA_SEP

            + CONSTRAINT    + " "   + GeofertasDBContract.FK_ADVERTISEMENT_TAG_ADVERTISEMENT    + " "
                    + FOREIGN_KEY   + " ("  + GeofertasDBContract.AdvertisementTag._ID              + ") "
                    + REFERENCES    + " "   + GeofertasDBContract.Advertisement.TABLE_NAME  + " ("  + GeofertasDBContract.Advertisement._ID     + ")"   + COMMA_SEP
            + CONSTRAINT    + " "   + GeofertasDBContract.FK_ADVERTISEMENT_TAG                   + " "
                    + FOREIGN_KEY   + " ("  + GeofertasDBContract.AdvertisementTag.TAG_ID_COLUMN    + ") "
                    + REFERENCES    +  " "  + GeofertasDBContract.Tag.TABLE_NAME            + " ("  + GeofertasDBContract.Tag._ID
                    + ")"   + ")"   + SEMICOLON_SEP;

    /*
    CREATE TABLE "Advertisement_Branch" (
"id" INTEGER NOT NULL,
"advertisement_id" INTEGER NOT NULL,
"branch_id" INTEGER NOT NULL,

PRIMARY KEY ("id") ,

CONSTRAINT "fk_advertisement_branch_advertisement"
FOREIGN KEY ("advertisement_id")
REFERENCES "Advertisement" ("id"),

CONSTRAINT "fk_advertisement_branch_branch"
FOREIGN KEY ("branch_id")
REFERENCES "Branch_Office" ("id")
);
     */
    private static final String CREATE_ADVERTISEMENT_BRANCH_TABLE =
            CREATE_TABLE_STATEMENT  + " "   + GeofertasDBContract.AdvertisementBranch.TABLE_NAME    + " ("

            + GeofertasDBContract.AdvertisementBranch._ID                       + " "   + INTEGER_TYPE  + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.AdvertisementBranch.ADVERTISEMENT_ID_COLUMN   + " "   + INTEGER_TYPE  + " "   + NOT_NULL  + COMMA_SEP
            + GeofertasDBContract.AdvertisementBranch.BRANCH_ID_COLUMN          + " "   + INTEGER_TYPE  + " "   + NOT_NULL  + COMMA_SEP

            + PRIMARY_KEY   + " ("  + GeofertasDBContract.AdvertisementBranch._ID   + ") "  + COMMA_SEP

            + CONSTRAINT    + " "   + GeofertasDBContract.FK_ADVERTISEMENT_BRANCH_ADVERTISEMENT     + " "
            + FOREIGN_KEY   + " ("  + GeofertasDBContract.AdvertisementBranch.ADVERTISEMENT_ID_COLUMN   + ") "
            + REFERENCES    + " "   + GeofertasDBContract.Advertisement.TABLE_NAME  + "("   + GeofertasDBContract.Advertisement._ID     + ")"   + COMMA_SEP

            + CONSTRAINT    + " "   + GeofertasDBContract.FK_ADVERTISEMENT_BRANCH_BRANCH    + " "
            + FOREIGN_KEY   + " ("  + GeofertasDBContract.AdvertisementBranch.BRANCH_ID_COLUMN  + ") "
            + REFERENCES    + " "   + GeofertasDBContract.BranchOffice.TABLE_NAME   + " ("  + GeofertasDBContract.BranchOffice._ID  + ")"
            + ")"   + SEMICOLON_SEP;

    /**
     * Create
     */
    private static final String SQL_CREATE_ENTRIES =
            CREATE_USER_TABLE_STATEMENT             +
            CREATE_ADVERTISEMENT_TABLE_STATEMENT    +
            CREATE_PARAMETER_TABLE_STATEMENT        +
            CREATE_TAG_TABLE_STATEMENT              +
            CREATE_COMPANY_TABLE_STATEMENT          +
            CREATE_BRANCH_OFFICE_TABLE_STATEMENT    +
            CREATE_ADVERTISEMENT_TAG_TABLE          +
            CREATE_ADVERTISEMENT_BRANCH_TABLE;


    /**
     * Sentencia delete
     */
    private static final String SQL_DELETE_ENTRIES =
            DROP_TABLE_STATEMENT + GeofertasDBContract.AdvertisementTag.TABLE_NAME + SEMICOLON_SEP +
            DROP_TABLE_STATEMENT + GeofertasDBContract.AdvertisementBranch.TABLE_NAME + SEMICOLON_SEP +
            DROP_TABLE_STATEMENT + GeofertasDBContract.BranchOffice.TABLE_NAME + SEMICOLON_SEP +
            DROP_TABLE_STATEMENT + GeofertasDBContract.User.TABLE_NAME + SEMICOLON_SEP +
            DROP_TABLE_STATEMENT + GeofertasDBContract.Advertisement.TABLE_NAME + SEMICOLON_SEP +
            DROP_TABLE_STATEMENT + GeofertasDBContract.Parameter.TABLE_NAME + SEMICOLON_SEP +
            DROP_TABLE_STATEMENT + GeofertasDBContract.Tag.TABLE_NAME + SEMICOLON_SEP +
            DROP_TABLE_STATEMENT + GeofertasDBContract.Company.TABLE_NAME + SEMICOLON_SEP;

    /**
     * Constructor
     * @param context
     */
    public GeofertasDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    /**
     * @param db
     */
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    /**
     * http://developer.android.com/training/basics/data-storage/databases.html
     * Método heredado de la clase padre
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    /**
     * Performs a database's downgrade
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }


}
