package ec.com.geofertas.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ec.com.geofertas.R;
import ec.com.geofertas.app.drawer.AdvertisementsListItem;

/**
 * Created by mac on 25/2/16.
 */
public class AdvertisementsListItemAdapter extends ArrayAdapter<AdvertisementsListItem> {

    // Application context
    Context mContext;
    // Custom drawer list items to bind to the views
    List<AdvertisementsListItem> mAdvertisementsItemList;
    // The resource ID for a layout file containing a DrawerItem to use when instantiating views.
    int mLayoutResID;

    public AdvertisementsListItemAdapter(Context context, int resource, List<AdvertisementsListItem> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mAdvertisementsItemList = objects;
        this.mLayoutResID = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        AdvertisementsItemHolder advertisementsItemHolder;
        View view = convertView;
        if(view == null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();

            view = inflater.inflate(mLayoutResID, parent, false);

            advertisementsItemHolder = new AdvertisementsItemHolder(view);

            view.setTag(advertisementsItemHolder);

        }
        else{
            advertisementsItemHolder = (AdvertisementsItemHolder) view.getTag();
        }

        // Then remakes the view
        AdvertisementsListItem advertisementsListItem = this.mAdvertisementsItemList.get(position);

        advertisementsItemHolder.advertisementIdTextView.setText(String.valueOf(advertisementsListItem.getId()));
        advertisementsItemHolder.iconView.setImageResource((advertisementsListItem.getIcon()));
        advertisementsItemHolder.advertisementTitleTextView.setText(advertisementsListItem.getAdvertisementTitle());

        return view;

    }

    // View holder
    // A ViewHolder object stores each of the component views inside the tag field of the Layout,
    // so you can immediately access them without the need to look them up repeatedly
    public static class AdvertisementsItemHolder {
        ImageView iconView;
        TextView advertisementTitleTextView;
        TextView advertisementIdTextView;

        public AdvertisementsItemHolder(View view) {
            advertisementIdTextView = (TextView) view.findViewById(R.id.advertisement_list_item_id_textview);
            iconView = (ImageView) view.findViewById(R.id.advertisement_list_item_image_imageview);
            advertisementTitleTextView = (TextView) view.findViewById(R.id.advertisement_list_item_title_textview);
        }
    }
}
