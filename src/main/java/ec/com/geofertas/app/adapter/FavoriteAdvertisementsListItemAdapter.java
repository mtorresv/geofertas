package ec.com.geofertas.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import ec.com.geofertas.app.drawer.FavoriteAdvertisementsListItem;
import ec.com.geofertas.R;

import java.util.List;

/**
 * Created by seymourdiera on 11/30/15.
 * Custom adapter that binds the list items for each view in the navigation drawer
 */
public class FavoriteAdvertisementsListItemAdapter extends ArrayAdapter<FavoriteAdvertisementsListItem> {

    // Application context
    Context mContext;
    // Custom drawer list items to bind to the views
    List<FavoriteAdvertisementsListItem> mFavoriteAdvertisementsItemList;
    // The resource ID for a layout file containing a DrawerItem to use when instantiating views.
    int mLayoutResID;


    /**
     * Constructor
     * @param context The current application context
     * @param layoutResourceID The resource ID for a layout file containing a DrawerItem to use when
     *                 instantiating views.
     * @param listItems The objects to represent in the NavigationDrawerList View.
     */
    public FavoriteAdvertisementsListItemAdapter(Context context, int layoutResourceID, List<FavoriteAdvertisementsListItem> listItems){
        super(context, layoutResourceID, listItems);
        this.mContext = context;
        this.mFavoriteAdvertisementsItemList = listItems;
        this.mLayoutResID = layoutResourceID;
    }


    // Get a View that displays the data at the specified position in the data set.
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        FavoriteAdvertisementsItemHolder favoriteAdvertisementsItemHolder;
        // You can either create a View manually or inflate it from an XML layout file.
        View view = convertView;
        // If the view is not inflated yet, we perform the process of inflating and adding the viewHolder to the tag property
        if(view == null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();

            view = inflater.inflate(mLayoutResID, parent, false);

            favoriteAdvertisementsItemHolder = new FavoriteAdvertisementsItemHolder(view);

            view.setTag(favoriteAdvertisementsItemHolder);

        }
        // Else, retrieves the holder from the tag
        else{
            favoriteAdvertisementsItemHolder = (FavoriteAdvertisementsItemHolder) view.getTag();
        }

        // Then remakes the view
        FavoriteAdvertisementsListItem favoriteAdvertisementsListItem = this.mFavoriteAdvertisementsItemList.get(position);

        favoriteAdvertisementsItemHolder.advertisementIdTextView.setText(String.valueOf(favoriteAdvertisementsListItem.getId()));
        favoriteAdvertisementsItemHolder.iconView.setImageResource((favoriteAdvertisementsListItem.getIcon()));
        favoriteAdvertisementsItemHolder.advertisementTitleTextView.setText(favoriteAdvertisementsListItem.getAdvertisementTitle());
        favoriteAdvertisementsItemHolder.advertisementCompanyTextView.setText(favoriteAdvertisementsListItem.getCompanyName());

        return view;

    }

    // View holder
    // A ViewHolder object stores each of the component views inside the tag field of the Layout,
    // so you can immediately access them without the need to look them up repeatedly
    public static class FavoriteAdvertisementsItemHolder {
        ImageView iconView;
        TextView advertisementTitleTextView;
        TextView advertisementCompanyTextView;
        TextView advertisementIdTextView;

        public FavoriteAdvertisementsItemHolder(View view) {
            advertisementIdTextView = (TextView) view.findViewById(R.id.favorite_advertisement_list_item_id_textview);
            iconView = (ImageView) view.findViewById(R.id.favorite_advertisement_list_item_image_imageview);
            advertisementTitleTextView = (TextView) view.findViewById(R.id.favorite_advertisement_list_item_title_textview);
            advertisementCompanyTextView = (TextView) view.findViewById(R.id.favorite_advertisement_list_item_company_textview);

        }
    }
}
