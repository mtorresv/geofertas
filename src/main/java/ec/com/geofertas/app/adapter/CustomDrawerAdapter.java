package ec.com.geofertas.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import ec.com.geofertas.app.drawer.DrawerItem;
import ec.com.geofertas.app.entities.User;
import ec.com.geofertas.R;

import java.util.List;

/**
 * Created by seymourdiera on 11/30/15.
 * Custom adapter that binds the list items for each view in the navigation drawer
 */
public class CustomDrawerAdapter extends ArrayAdapter<DrawerItem> {

    // Application context
    Context mContext;
    // Custom drawer list items to bind to the views
    List<DrawerItem> mDrawerItemList;
    // The resource ID for a layout file containing a DrawerItem to use when instantiating views.
    int mLayoutResID;

    int userInfoLayoutResourceId;

    User mUser;

    private static final String MY_PROFILE_TAG = "PRO";


    private static final int VIEW_TYPE_USER_INFO = 0;
    private static final int VIEW_TYPE_OPTION = 1;


    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == 0) ? VIEW_TYPE_USER_INFO : VIEW_TYPE_OPTION;
    }

    /**
     * Constructor
     * @param context The current application context
     * @param layoutResourceID The resource ID for a layout file containing a DrawerItem to use when
     *                 instantiating views.
     * @param listItems The objects to represent in the NavigationDrawerList View.
     */
    public CustomDrawerAdapter(Context context, int layoutResourceID, int userInfoLayoutResourceId, List<DrawerItem> listItems, User user){
        super(context, layoutResourceID, listItems);
        this.mContext = context;
        this.mDrawerItemList = listItems;
        this.mLayoutResID = layoutResourceID;
        this.mUser = user;
        this.userInfoLayoutResourceId = userInfoLayoutResourceId;
    }


    // Get a View that displays the data at the specified position in the data set.
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DrawerItemHolder drawerItemHolder;
        // You can either create a View manually or inflate it from an XML layout file.
        View view = convertView;
        // If the view is not inflated yet, we perform the process of inflating and adding the viewHolder to the tag property
        if(view == null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();

            if(mUser != null && position == 0) {
                view = inflater.inflate(userInfoLayoutResourceId, parent, false);
                drawerItemHolder = new DrawerItemHolder(view, VIEW_TYPE_USER_INFO);
            }else{
                view = inflater.inflate(mLayoutResID, parent, false);
                drawerItemHolder = new DrawerItemHolder(view, VIEW_TYPE_OPTION);
            }

            view.setTag(drawerItemHolder);

        }
        // Else, retrieves the holder from the tag
        else{
            drawerItemHolder = (DrawerItemHolder) view.getTag();
        }
        // Then remakes the view
        DrawerItem drawerItem =  this.mDrawerItemList.get(position);;
        if(mUser != null && position == 0) {

            Bitmap bmp = BitmapFactory.decodeByteArray(mUser.getImage(), 0, mUser.getImage().length);
            drawerItemHolder.userImageView.setImageBitmap(bmp);
            drawerItemHolder.tagView.setText(MY_PROFILE_TAG);
            drawerItemHolder.userNameTextView.setText(mUser.getFirstName() + " " + mUser.getLastname());
            drawerItemHolder.emailTextView.setText(mUser.getEmail());
        }else {
            drawerItemHolder.iconView.setImageDrawable(view.getResources().getDrawable(drawerItem.getIcon()));
            drawerItemHolder.textView.setText(drawerItem.getName());
            drawerItemHolder.tagView.setText(drawerItem.getNemonic());
        }
        return view;

    }



    // View holder
    // A ViewHolder object stores each of the component views inside the tag field of the Layout,
    // so you can immediately access them without the need to look them up repeatedly
    public static class DrawerItemHolder {
        ImageView iconView;
        TextView textView;
        TextView tagView;

        // User info layout
        ImageView userImageView;
        TextView userNameTextView;
        TextView emailTextView;

        public DrawerItemHolder(View view, int viewType) {
            tagView = (TextView) view.findViewById(R.id.navigation_drawer_tag_textview);
            if(viewType == VIEW_TYPE_USER_INFO){
                userImageView = (ImageView) view.findViewById(R.id.user_imageview);
                userImageView = (ImageView) view.findViewById(R.id.user_imageview);
                userNameTextView = (TextView) view.findViewById(R.id.user_name_textview);
                emailTextView = (TextView) view.findViewById(R.id.email_text_view);
            }else {
                iconView = (ImageView) view.findViewById(R.id.navigation_drawer_item_imageview);
                textView = (TextView) view.findViewById(R.id.navigation_drawer_item_textview);
            }
        }
    }
}
