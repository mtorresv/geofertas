package ec.com.geofertas.app.entities;

/**
 * Created by whoami on 12/13/15.
 */
public class User{

    private String username;
    private String lastname;
    private String firstName;
    private String email;
    private String passsword;
    private String authenticationType;
    private byte[] image;

    public User(byte[] image, String username, String lastname, String firstName, String email, String passsword, String authenticationType) {
        this.image = image;
        this.username = username;
        this.lastname = lastname;
        this.firstName = firstName;
        this.email = email;
        this.passsword = passsword;
        this.authenticationType = authenticationType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasssword() {
        return passsword;
    }

    public void setPasssword(String passsword) {
        this.passsword = passsword;
    }

    public String getAuthenticationType() {
        return authenticationType;
    }

    public void setAuthenticationType(String authenticationType) {
        this.authenticationType = authenticationType;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
