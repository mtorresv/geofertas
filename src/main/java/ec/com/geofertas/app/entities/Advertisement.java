package ec.com.geofertas.app.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by whoami on 1/7/16.
 * Clase que representa una oferta
 */
public class Advertisement implements Parcelable{

    private Long id;
    private String title;
    private String desciption;
    private Date startDate;
    private Date endDate;
    private byte[] image;

    public Advertisement(Long id, String title, String desciption, Date startDate, Date endDate, byte[] image) {
        this.id = id;
        this.title = title;
        this.desciption = desciption;
        this.startDate = startDate;
        this.endDate = endDate;
        this.image = image;
    }

    private Advertisement(Parcel in){
        this.id = in.readLong();
        this.title = in.readString();
        this.desciption = in.readString();
        this.startDate = null;
        this.endDate = null;
        this.image = null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesciption() {
        return desciption;
    }

    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        /**
         * private Long id;
         private String title;
         private String desciption;
         private Date startDate;
         private Date endDate;
         private byte[] image;
         */
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeString(desciption);
        dest.writeString(startDate.toString());
        dest.writeString(endDate.toString());
        dest.writeByteArray(image);
    }

    public static final Parcelable.Creator<Advertisement> CREATOR = new Parcelable.Creator<Advertisement>() {

        @Override
        public Advertisement createFromParcel(Parcel source) {
            return new Advertisement(source);
        }

        @Override
        public Advertisement[] newArray(int size) {
            return new Advertisement[size];
        }
    };
}
