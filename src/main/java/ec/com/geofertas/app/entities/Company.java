package ec.com.geofertas.app.entities;

/**
 * Created by mac on 22/2/16.
 */
public class Company {

    private Long id;
    private String name;
    private String description;
    private byte[] logo;

    public Company(Long id, String name, String description, byte[] logo) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.logo = logo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }
}
