package ec.com.geofertas.app.entities;

import java.util.List;

/**
 * Created by mac on 21/2/16.
 * Clase que representa a una sucursal
 */
public class BranchOffice {

    private Long id;
    private String name;
    private String address;
    private String telephone;
    private Double latitude;
    private Double longitude;
    private Company company;
    private List<Advertisement> advertisements;


    public BranchOffice(Long id, String name, String address, String telephone, Double latitude, Double longitude, Company company, List<Advertisement> advertisements) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.telephone = telephone;
        this.latitude = latitude;
        this.longitude = longitude;
        this.company = company;
        this.advertisements = advertisements;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<Advertisement> getAdvertisements() {
        return advertisements;
    }

    public void setAdvertisements(List<Advertisement> advertisements) {
        this.advertisements = advertisements;
    }
}
