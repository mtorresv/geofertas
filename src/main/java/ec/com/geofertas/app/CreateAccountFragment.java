package ec.com.geofertas.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import ec.com.geofertas.app.entities.User;
import ec.com.geofertas.app.service.GeofertasService;
import ec.com.geofertas.app.service.Impl.GeofertasServiceImpl;
import ec.com.geofertas.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 *
 */
public class CreateAccountFragment extends android.app.Fragment {

    private static final int SELECT_IMAGE = 1;

    Button createAccountButton;
    EditText usernameEditText;
    EditText nameEditText;
    EditText lastnameEditText;
    EditText emailEditText;
    EditText passwordEditText;
    ImageView profilePictureImageView;
    Button changeProfilePictureButton;
    // Service
    GeofertasService geofertasService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_account, container, false);

        usernameEditText = (EditText) view.findViewById(R.id.fragment_create_account_username_edittext);
        nameEditText = (EditText) view.findViewById(R.id.fragment_create_account_name_edittext);
        lastnameEditText = (EditText) view.findViewById(R.id.fragment_create_account_lastname_edittext);
        emailEditText = (EditText) view.findViewById(R.id.fragment_create_account_email_edittext);
        passwordEditText = (EditText) view.findViewById(R.id.fragment_create_account_password_edittext);
        profilePictureImageView = (ImageView) view.findViewById(R.id.fragment_create_account_profile_photo_imageview);
        changeProfilePictureButton = (Button) view.findViewById(R.id.fragment_create_account_change_photo_button);

        geofertasService = new GeofertasServiceImpl(getActivity());

        createAccountButton = (Button) view.findViewById(R.id.fragment_create_account_create_account_button);
        createAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (allFieldsValid()) {
                    createAccount();
                }
            }
        });

        changeProfilePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);//
                startActivityForResult(Intent.createChooser(intent, "Seleccione una imagen"), SELECT_IMAGE);
            }
        });

        return view;

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_IMAGE) {
            if (resultCode == getActivity().RESULT_OK) {
                if (data != null) {
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                        profilePictureImageView.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else if (resultCode == getActivity().RESULT_CANCELED) {
                    Toast.makeText(getActivity(), "Operación cancelada", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private boolean allFieldsValid() {
        boolean valid = true;
        if (usernameEditText.getText() == null || usernameEditText.getText().toString().trim().length() == 0) {
            usernameEditText.setError("El campo username es obligatorio");
            valid = false;
        }
        if (nameEditText.getText() == null || nameEditText.getText().toString().trim().length() == 0) {
            nameEditText.setError("El campo nombre es obligatorio");
            valid = false;
        }
        if (lastnameEditText.getText() == null || lastnameEditText.getText().toString().trim().length() == 0) {
            lastnameEditText.setError("El campo apellido es obligatorio");
            valid = false;
        }
        if (emailEditText.getText() == null || emailEditText.getText().toString().trim().length() == 0) {
            emailEditText.setError("El campo email es obligatorio");
            valid = false;
        }
        if (passwordEditText.getText() == null || passwordEditText.getText().toString().trim().length() == 0) {
            passwordEditText.setError("El campo password es obligatorio");
            valid = false;
        }
        return valid;
    }

    private void createAccount(){

        Bitmap bitmap = ((BitmapDrawable)profilePictureImageView.getDrawable()).getBitmap();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] image = bos.toByteArray();

        String username = usernameEditText.getText().toString();
        String name = nameEditText.getText().toString();
        String lastname = lastnameEditText.getText().toString();
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        User user = new User(image, username, lastname, name, email, password, "N");
        new CreateAccountTask().execute(user);
    }

    private class CreateAccountTask extends AsyncTask<User, Void, User> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(CreateAccountFragment.this.getActivity(), "", "Espere...");
        }

        @Override
        protected User doInBackground(User... params) {
            return geofertasService.createAccount(params[0]);
        }
        protected void onPostExecute(User user) {
            dialog.dismiss();
            if(user == null){
                Toast.makeText(getActivity(), "Error al crear la cuenta, intente nuevamente", Toast.LENGTH_SHORT).show();
            }
            else{
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                preferences.edit().putBoolean("isLogin", true).commit();
                Toast.makeText(getActivity(), "Su cuenta se ha creado satisfactoriamente", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                getActivity().startActivity(intent);
            }
        }
    }
}
