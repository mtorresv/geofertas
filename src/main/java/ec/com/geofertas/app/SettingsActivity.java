package ec.com.geofertas.app;

import android.app.FragmentTransaction;
import android.os.Bundle;

import ec.com.geofertas.R;

public class SettingsActivity extends DrawerActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        super.set();

        if (findViewById(R.id.activity_settings_content_frame) != null) {
            if(savedInstanceState != null){
                return;
            }
            // Initial fragment transaction for placing the settings fragment
            SettingsFragment settingsFragment = new SettingsFragment();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.activity_settings_content_frame, settingsFragment);
            //fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

}
