package ec.com.geofertas.app;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import ec.com.geofertas.R;
import ec.com.geofertas.app.drawer.AdvertisementsListItem;
import ec.com.geofertas.app.entities.Advertisement;
import ec.com.geofertas.app.entities.BranchOffice;
import ec.com.geofertas.app.entities.User;
import ec.com.geofertas.app.service.GeofertasService;
import ec.com.geofertas.app.service.Impl.GeofertasServiceImpl;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;


public class MapActivity extends DrawerActivity implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, LocationListener, ActivityCompat.OnRequestPermissionsResultCallback, GoogleMap.OnMarkerClickListener {

    Logger logger = Logger.getLogger(MapActivity.class.getName());

    GoogleApiClient mGoogleApiClient;

    GoogleMap mGoogleMap;

    LocationRequest mLocationRequest;

    Location mCurrentLocation;

    String mLastUpdateTime;

    GeofertasService geofertasService;

    List<BranchOffice> mBranchOffices;

    private static final int REQUEST_ACCESS_FINE_LOCATION_CODE = 1002;

    // Request code to use when launching the resolution activity
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    // Unique tag for the error dialog fragment
    private static final String DIALOG_ERROR = "dialog_error";
    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError = false;

    private static final String STATE_RESOLVING_ERROR = "resolving_error";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        set();

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mResolvingError = savedInstanceState != null
                && savedInstanceState.getBoolean(STATE_RESOLVING_ERROR, false);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        geofertasService = new GeofertasServiceImpl(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mGoogleApiClient.isConnected()){
            startLocationUpdates();
        }
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        mGoogleMap = map;
        // Check the permission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // If the permission is not granted
            // we can request the permission.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_ACCESS_FINE_LOCATION_CODE);
        } else {
          // TODO Is the permission already granted
            enableMyLocation();
        }

    }

    private void enableMyLocation(){
        // if the permission is granted
        mGoogleMap.setMyLocationEnabled(true);
        //mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
    }


    private void requestAdvertisements(){
        BranchOfficesTask branchOfficesTask = new BranchOfficesTask();
        Double radius = (double) getRadius();
        branchOfficesTask.execute(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), radius);
        /*final LatLng fybecaLatLng = new LatLng(-0.202204, -78.486778);
        mGoogleMap.addMarker(new MarkerOptions().position(fybecaLatLng).snippet("Fybeca"));

        final LatLng isveglioLatLng = new LatLng(-0.2038869, -78.485339);
        mGoogleMap.addMarker(new MarkerOptions().position(isveglioLatLng).snippet("Isveglio"));

        final LatLng sanasanaLatLng = new LatLng(-0.202649, -78.484710);
        mGoogleMap.addMarker(new MarkerOptions().position(sanasanaLatLng).snippet("Sana Sana"));
*/
        /*mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker != null) {
                    LayoutInflater layoutInflater = LayoutInflater.from(MapActivity.this);
                    final View view = layoutInflater.inflate(R.layout.advertisement_alert, null);
                    TextView advertisementTitleTextView = (TextView) view.findViewById(R.id.advertisement_title_textview);
                    TextView advertisementDescriptionTextView = (TextView) view.findViewById(R.id.advertisement_description_textview);
                    ImageView advertisementImageImageView = (ImageView) view.findViewById(R.id.advertisement_image_textview);
                    TextView seeMoreTextView = (TextView) view.findViewById(R.id.see_more_textview);
                    if (fybecaLatLng.equals(marker.getPosition())) {
                        advertisementTitleTextView.setText("Oferta en Línea Eucerín");
                        advertisementDescriptionTextView.setText("Comprando Eucerín, 25% de descuento en todos los productos antiedad.");
                        advertisementImageImageView.setImageResource(R.drawable.ic_eucerin);
                        seeMoreTextView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(MapActivity.this, AdvertisementDetailActivity.class);
                                MapActivity.this.startActivity(intent);

                            }
                        });
                    }
                    if (sanasanaLatLng.equals(marker.getPosition())) {
                        advertisementTitleTextView.setText("Nutricalcin");
                        advertisementDescriptionTextView.setText("20% de descuento");
                        advertisementImageImageView.setImageResource(R.drawable.ic_nutricalcin);
                        seeMoreTextView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(MapActivity.this, AdvertisementDetailActivity.class);
                                MapActivity.this.startActivity(intent);
                            }
                        });
                    }
                    if (isveglioLatLng.equals(marker.getPosition())) {
                        advertisementTitleTextView.setText("Descuentos USFQ Alumni");
                        advertisementDescriptionTextView.setText("Disfruta de este increible beneficio solo en Isveglio.");
                        advertisementImageImageView.setImageResource(R.drawable.ic_usfq_alumni);
                        seeMoreTextView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(MapActivity.this, AdvertisementDetailActivity.class);
                                MapActivity.this.startActivity(intent);
                            }
                        });
                    }


                    AlertDialog.Builder alertadd = new AlertDialog.Builder(MapActivity.this);
                    alertadd.setView(view);
                    alertadd.show();
                }
                return false;
            }
        });*/
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        // Connected to Google Play services!
        logger.info("Connected to Google Play Services!!!");

        // Para requerir actualizaciones automaticas de localizacion por determinados periodos de tiempo
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // descomentar para activar localizacion
        startLocationUpdates();
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mBranchOffices = null;
        mGoogleMap.clear();
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        /*if(mCurrentLocation != null){
            CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(17);
            mGoogleMap.moveCamera(center);
            mGoogleMap.animateCamera(zoom);
        }*/
        requestAdvertisements();
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection has been interrupted.
        // Disable any UI components that depend on Google APIs
        // until onConnected() is called.
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // This callback is important for handling errors that
        // may occur while attempting to connect with Google.
        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (result.hasResolution()) {
            try {
                mResolvingError = true;
                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GoogleApiAvailability.getErrorDialog()
            showErrorDialog(result.getErrorCode());
            mResolvingError = true;
        }

    }

    /* Creates a dialog for an error message */
    private void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getFragmentManager(), "errordialog");
    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mResolvingError) {
            mGoogleApiClient.connect();
        }

    }

    @Override
    protected void onStop() {
        if(mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    protected void stopLocationUpdates(){
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }



    /* A fragment to display an error dialog */
    public static class ErrorDialogFragment extends DialogFragment {
        public ErrorDialogFragment() { }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Get the error code and retrieve the appropriate dialog
            int errorCode = this.getArguments().getInt(DIALOG_ERROR);
            return GoogleApiAvailability.getInstance().getErrorDialog(
                    this.getActivity(), errorCode, REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ((MapActivity) getActivity()).onDialogDismissed();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(STATE_RESOLVING_ERROR, mResolvingError);
        //outState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, mRequestingLocationUpdates);
        //outState.putParcelable(LOCATION_KEY, mCurrentLocation);
        //outState.putString(LAST_UPDATED_TIME_STRING_KEY, mLastUpdateTime);
        super.onSaveInstanceState(outState);
    }

    /*
    Once the user completes the resolution provided by startResolutionForResult()
    or GoogleApiAvailability.getErrorDialog(), your activity receives the onActivityResult()
    callback with the RESULT_OK result code. You can then call connect() again. For example:
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_ACCESS_FINE_LOCATION_CODE) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                enableMyLocation();
                requestAdvertisements();
            } else {
                // Permission was denied. Display an error message.
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Geofertas necesita acceder a tu localizacion para funcionar. Por favor reinicia la aplicacion y otorga el permiso respectivo.");
                builder.create();
                builder.show();
            }
        }
    }

    private class BranchOfficesTask extends AsyncTask<Double, Void, List<BranchOffice>> {
        //ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            //dialog = ProgressDialog.show(MapActivity.this, "", "Recibiendo información...");
        }

        @Override
        protected List<BranchOffice> doInBackground(Double... params) {
            return geofertasService.getBranchOffices(params[0], params[1], params[2]);
        }
        protected void onPostExecute(List<BranchOffice> branchOffices) {
            //dialog.dismiss();
            if(branchOffices == null){
                Toast.makeText(MapActivity.this, "No hay información disponible", Toast.LENGTH_SHORT).show();
            }
            else{
                mBranchOffices = branchOffices;
                for(BranchOffice branchOffice : mBranchOffices){
                    LatLng latLng = new LatLng(branchOffice.getLatitude(), branchOffice.getLongitude());
                    mGoogleMap.addMarker(new MarkerOptions().position(latLng));
                    //MarkerOptions markerOptions = new MarkerOptions().position(latLng);
                    /*if(branchOffice.getCompany().getLogo() != null){
                        BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeByteArray(branchOffice.getCompany().getLogo(), 0, branchOffice.getCompany().getLogo().length));
                        markerOptions.icon(bitmapDescriptor);
                    }*/
                    //mGoogleMap.addMarker(markerOptions);
                }
                mGoogleMap.setOnMarkerClickListener(MapActivity.this);
            }
        }
    }

    private int getRadius(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        return Integer.parseInt(preferences.getString("preference_radius", "100"));

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker != null) {
            for(BranchOffice branchOffice : mBranchOffices){
                if(branchOffice.getLatitude() != null && branchOffice.getLatitude().equals(marker.getPosition().latitude)
                        && branchOffice.getLongitude() != null && branchOffice.getLongitude().equals(marker.getPosition().longitude)){
                    Intent intent = new Intent(MapActivity.this, AdvertisementsListActivity.class);
                    intent.putParcelableArrayListExtra("ADVERTISEMENTS", new ArrayList<Parcelable>(branchOffice.getAdvertisements()));
                    MapActivity.this.startActivity(intent);
                    break;
                }
            }
            /*LayoutInflater layoutInflater = LayoutInflater.from(MapActivity.this);
            final View view = layoutInflater.inflate(R.layout.advertisement_alert, null);
            TextView advertisementTitleTextView = (TextView) view.findViewById(R.id.advertisement_title_textview);
            TextView advertisementDescriptionTextView = (TextView) view.findViewById(R.id.advertisement_description_textview);
            ImageView advertisementImageImageView = (ImageView) view.findViewById(R.id.advertisement_image_textview);
            TextView seeMoreTextView = (TextView) view.findViewById(R.id.see_more_textview);
            advertisementTitleTextView.setText("Oferta en Línea Eucerín");
            advertisementDescriptionTextView.setText("Comprando Eucerín, 25% de descuento en todos los productos antiedad.");
            advertisementImageImageView.setImageResource(R.drawable.ic_eucerin);
            seeMoreTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MapActivity.this, AdvertisementDetailActivity.class);
                    MapActivity.this.startActivity(intent);

                }
            });
            AlertDialog.Builder alertadd = new AlertDialog.Builder(MapActivity.this);
            alertadd.setView(view);
            alertadd.show();*/
        }
        return false;
    }
}
