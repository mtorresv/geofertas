package ec.com.geofertas.app;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;

import ec.com.geofertas.R;

public class HomeActivity extends DrawerActivity {

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        super.set();
        if(!isOnline()){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Geofertas necesita conexión a Internet para funcionar, verifica tu conexión e intenta nuevamente.")
                    .setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            onCreate(savedInstanceState);
                        }
                    })
                    .setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            HomeActivity.this.finish();
                        }
                    });
            builder.create();
            builder.show();
        }

        if (findViewById(R.id.activity_home_content_frame) != null) {
            if(savedInstanceState != null){
                return;
            }
            // Initial fragment transaction for placing the login fragment
            HomeFragment homeFragment = new HomeFragment();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.activity_home_content_frame, homeFragment);
            //fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        }
    }

}
