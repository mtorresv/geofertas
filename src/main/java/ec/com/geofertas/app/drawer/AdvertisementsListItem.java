package ec.com.geofertas.app.drawer;

/**
 * Created by mac on 25/2/16.
 * Clase que representa un objeto para mostrar en el listado de ofertas por sucursal
 */
public class AdvertisementsListItem {

    private int id;
    private int icon;
    private String advertisementTitle;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getAdvertisementTitle() {
        return advertisementTitle;
    }

    public void setAdvertisementTitle(String advertisementTitle) {
        this.advertisementTitle = advertisementTitle;
    }
}
