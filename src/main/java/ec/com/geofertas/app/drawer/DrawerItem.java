package ec.com.geofertas.app.drawer;

/**
 * Created by whoami on 11/22/15.
 */
public class DrawerItem {

    private String name;
    private String nemonic;
    private int icon;


    // User info layout
    private int userImage;
    private String userName;
    private String email;

    public DrawerItem() {
    }

    public DrawerItem(String name, String nemonic, int icon){
        this.name = name;
        this.nemonic = nemonic;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNemonic() {
        return nemonic;
    }

    public void setNemonic(String nemonic) {
        this.nemonic = nemonic;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getUserImage() {
        return userImage;
    }

    public void setUserImage(int userImage) {
        this.userImage = userImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
