package ec.com.geofertas.app.drawer;

/**
 * Created by whoami on 11/22/15.
 */
public class FavoriteAdvertisementsListItem {

    private int id;
    private int icon;
    private String companyName;
    private String advertisementTitle;

    public FavoriteAdvertisementsListItem(int id, int icon, String companyName, String advertisementTitle){
        this.id = id;
        this.icon = icon;
        this.companyName = companyName;
        this.advertisementTitle = advertisementTitle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAdvertisementTitle() {
        return advertisementTitle;
    }

    public void setAdvertisementTitle(String advertisementTitle) {
        this.advertisementTitle = advertisementTitle;
    }
}
