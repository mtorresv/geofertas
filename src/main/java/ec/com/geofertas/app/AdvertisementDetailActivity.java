package ec.com.geofertas.app;

import android.app.FragmentTransaction;
import android.os.Bundle;

import ec.com.geofertas.R;

public class AdvertisementDetailActivity extends DrawerActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertisement_detail);
        super.set();
        if (findViewById(R.id.activity_advertisemenet_detail_content_frame) != null) {
            if(savedInstanceState != null){
                return;
            }
            // Initial fragment transaction for placing the login fragment
            AdvertisementDetailFragment advertisementDetailFragment = new AdvertisementDetailFragment();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.activity_advertisemenet_detail_content_frame, advertisementDetailFragment);
            fragmentTransaction.commit();

        }
    }

}
