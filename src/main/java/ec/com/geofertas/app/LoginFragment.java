package ec.com.geofertas.app;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import ec.com.geofertas.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.SignInButton;

/**
 * @author whoami
 * @version 1.0
 *
 * Login fragment controller class, contains the following options:
 *  Google Account Login
 *  E-mail and password login
 *  (¿Don't have an account? Sign-up!)
 */
public class LoginFragment extends Fragment {

    Button createAccountButton;
    Button signInButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        // Binds the create account button from login fragment
        createAccountButton = (Button) view.findViewById(R.id.fragment_login_create_account_button);
        createAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                fragmentTransaction.hide(LoginFragment.this);
                fragmentTransaction.replace(R.id.activity_login_content_frame, new CreateAccountFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        signInButton = (Button) view.findViewById(R.id.fragment_login_sign_in_button);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
                fragmentTransaction.hide(LoginFragment.this);
                fragmentTransaction.replace(R.id.activity_login_content_frame, new SignInFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });





        return view;
    }
}
