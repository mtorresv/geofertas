package ec.com.geofertas.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ec.com.geofertas.app.adapter.CustomDrawerAdapter;
import ec.com.geofertas.app.database.GeofertasDBContract;
import ec.com.geofertas.app.database.GeofertasDbHelper;
import ec.com.geofertas.app.drawer.DrawerItem;
import ec.com.geofertas.app.entities.User;
import ec.com.geofertas.app.service.Impl.GeofertasServiceImpl;
import ec.com.geofertas.R;

import java.util.List;

/**
 * Clase controladora del activity principal
 *
 * @author mtorres
 */
public class DrawerActivity extends FragmentActivity {

    private static final String LOGIN_OPTION_TAG = "INI";
    private static final String EXPLORE_OPTION_TAG = "EXP";
    private static final String SETTINGS_OPTION_TAG = "OPT";
    private static final String FAVORITES_OPTION_TAG = "FAV";
    private static final String MY_PROFILE_OPTION_TAG = "PRO";
    private static final String HELP_OPTION_TAG = "HLP";


    // drawer layout object
    private DrawerLayout mDrawerLayout;

    // Listview of navigation drawer items to be presented
    ListView mDrawerList;

    protected RelativeLayout mCompleteLayout;
    protected RelativeLayout mActivityLayout;

    // nav drawer title
    private CharSequence mDrawerTitle;

    // used to store app title
    private CharSequence mTitle;

    ActionBarDrawerToggle mDrawerToggle;

    List<DrawerItem> mDrawerItemList;

    private CustomDrawerAdapter mDrawerItemAdapter;


    protected User mUser = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        set();
    }

    public boolean isAuthenticated(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        return preferences.getBoolean("isLogin", false);
    }

    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public void set(){

        // Sets the app title
        mTitle = mDrawerTitle = getTitle();

        // Binds the drawer layout to the object
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Binds the navigation drawer layout
        mDrawerList = (ListView) findViewById(R.id.left_drawer);


        // Gets the navigation drawer items from layout
        mDrawerItemList = new GeofertasServiceImpl(this).getMenuOptions(isAuthenticated());

        // Sets the onItemClickListener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // if is authenticated, retrieves user info from database
        if(isAuthenticated()){
            retrieveUser();
        }

        mDrawerItemAdapter = new CustomDrawerAdapter(
                this,
                R.layout.navigation_drawer_item,
                R.layout.user_info,
                mDrawerItemList,
                mUser
        );

        // Set the adapter for the list view
        mDrawerList.setAdapter(mDrawerItemAdapter);

        // enabling action bar app icon and behaving it as toggle button
        getActionBar().setDisplayHomeAsUpEnabled(Boolean.TRUE);
        getActionBar().setHomeButtonEnabled(Boolean.TRUE);
        getActionBar().setIcon(R.mipmap.ic_launcher);


        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /**
             * Called when a drawer has settled in a completely closed state.
             */
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /**
             * Called when a drawer has settled in a completely open state.
             */
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);


    }

    protected void retrieveUser() {
        GeofertasDbHelper geofertasDbHelper = new GeofertasDbHelper(this);
        SQLiteDatabase sqLiteDatabase = geofertasDbHelper.getReadableDatabase();
        String[] projection = {
                GeofertasDBContract.User._ID,
                GeofertasDBContract.User.PROFILE_PICTURE_COLUMN,
                GeofertasDBContract.User.USERNAME_COLUMN,
                GeofertasDBContract.User.LAST_NAME_COLUMN,
                GeofertasDBContract.User.FIRST_NAME_COLUMN,
                GeofertasDBContract.User.EMAIL_COLUMN,
                GeofertasDBContract.User.PASSWORD_COLUMN,
                GeofertasDBContract.User.AUTHENTICATION_TYPE_COLUMN
        };
        Cursor cursor = sqLiteDatabase.query(GeofertasDBContract.User.TABLE_NAME, projection, null, null, null, null, null);
        cursor.moveToFirst();
        // public User(String username, String lastname, String firstName, String email, String passsword, String authenticationType)
        byte[] profilePicture = cursor.getBlob(cursor.getColumnIndexOrThrow(GeofertasDBContract.User.PROFILE_PICTURE_COLUMN));
        String username = cursor.getString(cursor.getColumnIndexOrThrow(GeofertasDBContract.User.USERNAME_COLUMN));
        String lastname = cursor.getString(cursor.getColumnIndexOrThrow(GeofertasDBContract.User.LAST_NAME_COLUMN));
        String firstName = cursor.getString(cursor.getColumnIndexOrThrow(GeofertasDBContract.User.FIRST_NAME_COLUMN));
        String email = cursor.getString(cursor.getColumnIndexOrThrow(GeofertasDBContract.User.EMAIL_COLUMN));
        String password = cursor.getString(cursor.getColumnIndexOrThrow(GeofertasDBContract.User.PASSWORD_COLUMN));
        String authenticationType = cursor.getString(cursor.getColumnIndexOrThrow(GeofertasDBContract.User.AUTHENTICATION_TYPE_COLUMN));
        User user = new User(profilePicture, username, lastname, firstName, email, password, authenticationType);
        mUser = user;
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
//        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            selectItem(view, position);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    /**
     * Swaps fragments in the main content view
     */
    protected void selectItem(View view, int position) {


        TextView textView = (TextView) view.findViewById(R.id.navigation_drawer_tag_textview);

        final String text = textView.getText().toString();
        Intent intent = null;

        switch (text) {
            case MY_PROFILE_OPTION_TAG:
                intent = new Intent(this, ProfileActivity.class);
                break;
            case LOGIN_OPTION_TAG:
                intent = new Intent(this, LoginActivity.class);
                break;
            case EXPLORE_OPTION_TAG:
                intent = new Intent(this, MapActivity.class);
                break;
            case SETTINGS_OPTION_TAG:
                intent = new Intent(this, SettingsActivity.class);
                break;
            case FAVORITES_OPTION_TAG:
                intent = new Intent(this, FavoriteAdvertisementsActivity.class);
                break;
            case HELP_OPTION_TAG:
                intent = new Intent(this, HelpActivity.class);
                break;
            default:
                break;
        }
        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);
        if (intent != null && !this.getClass().getName().equals(intent.getComponent().getClassName())) {
            //intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        }
    }

}
