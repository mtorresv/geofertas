package ec.com.geofertas.app;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import ec.com.geofertas.R;


/**
 * Settings fragment
 * */
public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Loads the preferences from the xml resource
        addPreferencesFromResource(R.xml.preferences);
    }
}
