package ec.com.geofertas.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import ec.com.geofertas.app.adapter.FavoriteAdvertisementsListItemAdapter;
import ec.com.geofertas.app.service.Impl.GeofertasServiceImpl;
import ec.com.geofertas.R;

public class FavoriteAdvertisementsActivity extends DrawerActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_advertisements);
        super.set();

        ListView favoriteAdvertisementsListView = (ListView) findViewById(R.id.favorite_advertisements_listview);
        if(favoriteAdvertisementsListView != null){

            FavoriteAdvertisementsListItemAdapter favoriteAdvertisementsListItemAdapter = new FavoriteAdvertisementsListItemAdapter(this,
                    R.layout.favorite_advertisement_list_item,
                    new GeofertasServiceImpl(this).getFavoriteAdvertisements());
            favoriteAdvertisementsListView.setAdapter(favoriteAdvertisementsListItemAdapter);
            favoriteAdvertisementsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(FavoriteAdvertisementsActivity.this, AdvertisementDetailActivity.class);
                    startActivity(intent);
                }
            });
        }
    }

}
